const express = require('express'),
    app = express(),
    path = require('path'),
    https = require('https'),
    axios = require('axios'),
    publicPath = path.join(__dirname, 'public');
    PORT = process.env.PORT || 3000;

    require('dotenv').config();


app.get('/', function (req,res) {
    res.sendFile(path.join(publicPath, 'index.html'));
})

app.get('/query', async function (req,res) {
    var lat = req.query.lat;
    var lon = req.query.lon;

    var jsonData = {};

    try {
        const [ aqi , weather ] = await axios.all([
            axios.get('https://api.waqi.info/feed/geo:'+lat+';'+lon+'/?token='+process.env.TOKEN),
            axios.get('https://api.openweathermap.org/data/2.5/onecall?lat='+lat+'&lon='+lon+'&exclude=daily,minutely&lang=de&units=metric&appid='+process.env.API_KEY)
        ]);
        jsonData['aqi'] = aqi.data.data.aqi;
        jsonData['weather'] = parseWeather(weather.data)
    } catch (error) {
        console.log(error.response.body);
    }
    await res.json(jsonData)
})

app.use('/', express.static(publicPath));

app.listen(PORT);

function parseWeather(obj){
    var current = obj.current
    var obj1 = {}
    obj1['dt'] = current.dt;
    obj1['sunrise'] = current.sunrise;
    obj1['sunset'] = current.sunset;
    obj1['temp'] = current.temp;
    obj1['feels_like'] = current.feels_like;
    obj1['uvi'] = current.uvi;
    obj1['visibility'] = current.visibility;
    obj1['wind_speed'] = current.wind_speed;
    obj1['description'] = current.weather[0].description;
    obj1['icon'] = current.weather[0].icon;

    var hourly = []
    let i = 0;
    obj.hourly.forEach(hour => {
        if(i === 8) return
        if(hour.dt > current.dt){
            hourly.push(parseHour(hour))
            i++;
        }
    })
    obj1['hourly_forecast'] = hourly;

    return obj1
}

function parseHour(obj) {
    var obj1 = {}
    obj1['dt'] = obj.dt;
    obj1['temp'] = obj.temp;
    obj1['icon'] = obj.weather[0].icon;
    obj1['description'] = obj.weather[0].description;
    return obj1
}

