getData()
var contentElement = $('#content')

function getData(){
    navigator.geolocation.getCurrentPosition(setPosition, error)
}

function setPosition(position) {
    var lon = position.coords.longitude
    var lat = position.coords.latitude

    $.ajax({
        url: '/query?lon='+lon+'&lat='+lat,
        complete: function(data) {
            jsonObj = JSON.parse(data.responseText)

            dynamicAppearance(jsonObj)
            populateCurrent(jsonObj.weather)
            populateCollapse(jsonObj)
        }
    });
}

function error(error){
    document.body.style.backgroundImage = "url('/backgrounds/d.png')"
    alert("\"Wetter Aktuell\" needs your location to work.\n" +
        "Please allow the location and reload the page to use it.")
    console.log(error.message);
}

function dynamicAppearance(obj) {
    var t = obj.weather.icon.slice(-1)
    if(t === 'd'){
        if(document.body.classList.contains("text-white"))
            document.body.classList.remove("text-white")

        document.body.classList.add("text-dark")
    }else{
        if(document.body.classList.contains("text-dark"))
            document.body.classList.remove("text-dark")

        document.body.classList.add("text-white")
    }
    $("#favicon").attr("href","icons/32x32/"+obj.weather.icon+".png");
    document.body.style.backgroundImage = "url('/backgrounds/"+t+".png')"
}

function populateCurrent(obj){
    var item =
        '<div class="row">'+
        '<div class="col pb-5">'+
        '<div class="container text-center">'+
        '<div class="row">'+
        '<div class="col">'+
        '<img src="icons/256x256/'+obj.icon+'.png" alt="Wetter Icon">'+
        '</div>'+
        '</div>'+
        '<div class="row">'+
        '<div class="col">'+
        '<h1>'+obj.description+'</h1>'+
        '</div>'+
        '</div>'+
        '<div class="row">'+
        '<div class="col">'+
        '<h3>'+Math.round(obj.temp)+'°C</h3>'+
        '</div> </div> </div> </div> </div>';
    contentElement.append(item)
    populateHourly(obj.hourly_forecast)
}

function populateHourly(obj){
    var item = '<div class="row">';
    obj.forEach(hour => {
        item +=
            '<div class="col">'+
            '<div class="container" >'+
            '<div class="row">'+
            '<div class="col">'+
            '<p>'+unixTime(hour.dt)+'</p>'+
            '</div> </div>'+
            '<div class="row">'+
            '<div class="col">'+
            '<img src="icons/32x32/'+hour.icon+'.png" alt="'+hour.description+'">'+
            '</div> </div>'+
            '<div class="row">'+
            '<div class="col">'+
            '<p>'+Math.round(hour.temp)+'°C</p>'+
            '</div> </div> </div> </div>';
    })
    item += '</div>';
    contentElement.append(item)
}

function populateCollapse(obj) {
    var item =
        '<button class="btn '+(obj.weather.icon.slice(-1) === 'n' ? "btn-outline-light" : "btn-outline-dark")+'" type="button" data-toggle="collapse" data-target="#collapse" aria-expanded="false" aria-controls="collapse">Detials</button>'+
        '<div class="row">' +
        '<div class="collapse text-black" id="collapse">'+
        '<div class="card card-body">'+
        '<div class="row">'+
        aqi(obj.aqi)+
        uvi(obj.weather.uvi)+
        wind(obj.weather.wind_speed)+
        visibility(obj.weather.visibility)+
        feelsLike(obj.weather.feels_like)+
        sun(obj.weather.sunset, obj.weather.sunrise, obj.weather.dt)+
        '</div> </div> </div> </div>'
    contentElement.append(item)
}

function sun(sunset, sunrise, dt){
    if(dt >= sunrise && dt <= sunset){
        return detailHTMl('SONNE ↓', unixTime(sunset))
    } else {
        return detailHTMl('SONNE ↑', unixTime(sunrise))
    }
}

function feelsLike(feelsLike) {
    return detailHTMl('GEFÜHLT', Math.round(feelsLike)+"°C")
}

function visibility(visibility){
    return detailHTMl('SICHTWEITE', (visibility/1000)+" Km")
}

function wind(wind) {
    return detailHTMl('WIND', wind+" Km/h")
}

function uvi(uvi) {
    return detailHTMl('UV-INDEX',uvi)
}

function aqi(aqi){
    var val = aqi

    if(val <= 50){
        val += " - GUT";
    }else if(val >= 51 && val <= 100){
        val += " - MODERAT";
    }else if(val >= 101 && val <= 150){
        val += " - UNGESUND FÜR GEFÄHRDETE GRUPPEN"
    }else if(val >= 151 && val <= 200){
        val += " - UNGESUND"
    }else if(val >= 201 && val <= 300){
        val += " - SEHR UNGESUND"
    }else if(val > 300){
        val += " - GEFÄHRLICH"
    }

    return detailHTMl('LUFTQUALITÄT', val)
}

function detailHTMl(key, value){
    var item =
        '<div class="col mx-5 py-4">'+
        '<div class="row">'+
        '<div class="col">'+
        '<h4>'+key+'</h4>'+
        '</div>'+
        '</div>'+
        '<div class="row">'+
        '<div class="col">'+
        '<h4>'+value+'</h4>'+
        '</div> </div> </div>';
    return item
}

function unixTime(t){
    var dt = new Date(t*1000);
    return dt.getHours()+" UHR";
}
