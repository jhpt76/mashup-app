# Mashup-App

 My mashup app "Wetter Aktuell" shows all relevant data about the current temperature and weather at a glance. If desired, more detailed data such as the air quality index, the felt temperature and much more can be displayed. To display all this data I have connected 2 APIs, the Openweahter Map API and the API of the World Air Quality Index project. The combination of these two APIs allows me to provide more and more accurate data to the user. Air quality can play an important role in many regions, as excessive air pollution can lead to serious health problems.


### Installation

Wetter Aktuell requires Node.js

1. clone the project
2. go into the folder
3. install the dependencies
4. start the server




```sh
$ git clone git@git.thm.de:jhpt76/mashup-app.git
$ cd mashup-app
$ npm install
$ npm start
```

